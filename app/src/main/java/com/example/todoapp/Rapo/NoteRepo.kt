package com.example.todoapp.Rapo

import androidx.lifecycle.LiveData
import com.example.todoapp.ROOM_DB.NoteDao
import com.example.todoapp.ROOM_DB.Notes

class NoteRepo(private val noteDao: NoteDao) {
    val allNotes : LiveData<List<Notes>> = noteDao.getAllNotes()
    suspend fun insert(notes: Notes){
        noteDao.insert(notes)
    }
    suspend fun delete(notes: Notes){
        noteDao.delete(notes)
    }
}

