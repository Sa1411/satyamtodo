package com.example.todoapp.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.ROOM_DB.Notes
import kotlin.random.Random

class Notes_Items_Recycler_View_Adapter(val context: Context, val listner : iNotesRVAdapter) : RecyclerView.Adapter<Notes_Items_Recycler_View_Adapter.NotesViewHolder>() {

    val allnotes = ArrayList<Notes>()

    inner class NotesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val textview = itemView.findViewById<TextView>(R.id.notes_item)
        val delete_Button = itemView.findViewById<TextView>(R.id.delete_Notes)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val viewholder = NotesViewHolder(LayoutInflater.from(context).inflate(R.layout.notes_items,parent,false))


        viewholder.delete_Button.setOnClickListener {/*delete notes */
            listner.OnItemClicked(allnotes[viewholder.adapterPosition])
        }

        return viewholder
    }

    override fun getItemCount(): Int {
        return allnotes.size
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val currentnote = allnotes[position]
        holder.textview.text = currentnote.text
       // holder.delete_Button.setBackgroundColor(holder.itemView.resources.getColor(randomColour(),null))
        holder.textview.setBackgroundColor(holder.itemView.resources.getColor(randomColour(),null))

    }
    fun updateList(updatedList : List<Notes>){
        allnotes.clear()
        allnotes.addAll(updatedList)

        notifyDataSetChanged()
    }
}
fun randomColour():Int{
    val list = ArrayList<Int>()
    list.add(R.color.Colour1)
    list.add(R.color.Colour2)
    list.add(R.color.Colour3)
    list.add(R.color.Colour4)
    list.add(R.color.Colour5)
    list.add(R.color.Colour6)
    list.add(R.color.Colour7)
    val seed = System.currentTimeMillis().toInt()
    val randomIndex = Random(seed).nextInt(list.size)
    return list[randomIndex]
}

interface iNotesRVAdapter{
    fun OnItemClicked(notes: Notes)
}


